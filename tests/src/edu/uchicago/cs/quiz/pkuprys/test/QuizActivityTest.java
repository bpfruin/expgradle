package edu.uchicago.cs.quiz.pkuprys.test;


import android.app.Activity;
import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;

import edu.uchicago.cs.quiz.pkuprys.QuestionActivity;
import edu.uchicago.cs.quiz.pkuprys.QuizActivity;
import edu.uchicago.cs.quiz.pkuprys.QuizTracker;
import edu.uchicago.cs.quiz.pkuprys.R;


public class QuizActivityTest extends
        ActivityInstrumentationTestCase2<QuizActivity> {

    private Button mExitButton, mStartButton;
    private String mName;
    private EditText mNameEditText;
    private QuizTracker mQuizTracker;


    private Activity mActivity = null;


    public QuizActivityTest() {
        super(QuizActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        //this calls onCreate of the activity
        mActivity = getActivity();

        mExitButton = (Button) mActivity.findViewById(R.id.exitButton);
        mStartButton = (Button) mActivity.findViewById(R.id.startButton);
        mNameEditText = (EditText) mActivity.findViewById(R.id.editName);

        mQuizTracker = QuizTracker.getInstance();



    }

    public void testStartQuiz() {

        TouchUtils.tapView(this, mNameEditText);

        sendKeys(KeyEvent.KEYCODE_A);
        sendKeys(KeyEvent.KEYCODE_D);
        sendKeys(KeyEvent.KEYCODE_A);
        sendKeys(KeyEvent.KEYCODE_M);

        sendKeys(KeyEvent.KEYCODE_BACK);

        Watch.watch();
        Instrumentation.ActivityMonitor monitor =
                getInstrumentation().addMonitor(QuestionActivity.class.getName(), null, false);

        TouchUtils.clickView(this, mStartButton);

        Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5);

        assertEquals(nextActivity.getComponentName().getShortClassName(), ".QuestionActivity");

        assertEquals("ADAM", mQuizTracker.getName().toUpperCase());

        Watch.watch();
        nextActivity.finish();




    }







}
